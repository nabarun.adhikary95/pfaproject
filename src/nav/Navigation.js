import React from 'react';
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
import Home from '../Tabs/Home';
import Cradit from '../Tabs/Cradit';
import Favourite from '../Tabs/Favourite';
import Chats from '../Tabs/Chats';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
const Tab = createBottomTabNavigator();

function Mytabs() {
  const styles = StyleSheet.create({
    focusTabScreen: {
      backgroundColor: '#fff',
      borderRadius: 8,
      height: rh(5),
      width: rw(10),
      justifyContent: 'center',
      alignItems: 'center',
    },
    tabScreenWithoutFocusMain: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    TabTextView: {
      color: '#fff',
      marginTop: -3,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: '#fff',
        tabBarInactiveTintColor: '#065659',
        tabBarShowLabel: false,
        tabBarStyle: {
          height: 55,
          borderTopWidth: 1,
          paddingTop: rh(1),
          paddingBottom: rh(1),
          backgroundColor: '#065659',
          margin: 0,
          borderTopRightRadius: 30,
          borderTopLeftRadius: 30,
          borderBottomLeftRadius: 30,
          borderBottomRightRadius: 30,
          marginHorizontal: 15,
          position: 'absolute',
          overflow: 'hidden',
          left: 0,
          bottom: 0,
          right: 0,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({color, size, focused}) => {
            if (focused) {
              return (
                <View style={styles.focusTabScreen}>
                  <AntDesign name="home" size={rh(3.5)} color={'#065659'} />
                </View>
              );
            } else {
              return (
                <View style={styles.tabScreenWithoutFocusMain}>
                  <AntDesign name="home" size={20} color={'#fff'} />
                  <View
                    style={styles.TabTextView}>
                    <Text style={{color: '#fff', fontSize: rf(1.5)}}>home</Text>
                  </View>
                </View>
              );
            }
          },
        }}
      />
      <Tab.Screen
        name="Cradit"
        component={Cradit}
        options={{
          tabBarIcon: ({color, size, focused}) => {
            if (focused) {
              return (
                <View style={styles.focusTabScreen}>
                  <Ionicons
                    name="ios-speedometer-outline"
                    size={rh(3.5)}
                    color={'#065659'}
                  />
                </View>
              );
            } else {
              return (
                <View style={styles.tabScreenWithoutFocusMain}>
                  <Ionicons
                    name="ios-speedometer-outline"
                    size={20}
                    color={'#fff'}
                  />
                  <View
                    style={styles.TabTextView}>
                    <Text style={{color: '#fff', fontSize: rf(1.5)}}>
                      Cradit
                    </Text>
                  </View>
                </View>
              );
            }
          },
        }}
      />
      <Tab.Screen
        name="Favourite"
        component={Favourite}
        options={{
          tabBarIcon: ({color, size, focused}) => {
            if (focused) {
              return (
                <View style={styles.focusTabScreen}>
                  <Entypo
                    name="heart-outlined"
                    size={rh(3.5)}
                    color={'#065659'}
                  />
                </View>
              );
            } else {
              return (
                <View style={styles.tabScreenWithoutFocusMain}>
                  <Entypo name="heart-outlined" size={20} color={'#fff'} />
                  <View
                    style={styles.TabTextView}>
                    <Text style={{color: '#fff', fontSize: rf(1.5)}}>
                      Favourite
                    </Text>
                  </View>
                </View>
              );
            }
          },
        }}
      />
      <Tab.Screen
        name="ChatNavigation"
        component={Chats}
        options={{
          tabBarStyle : {display:"none"},
          tabBarIcon: ({color, size, focused}) => {
            if (focused) {
              return (
                <View style={styles.focusTabScreen}>
                  <Entypo name="chat" size={rh(3.5)} color={'#065659'} />
                </View>
              );
            } else {
              return (
                <View style={styles.tabScreenWithoutFocusMain}>
                  <Entypo name="chat" size={20} color={'#fff'} />
                  <View style={styles.TabTextView}>
                    <Text style={{color: '#fff', fontSize: rf(1.5)}}>
                      Chats
                    </Text>
                  </View>
                </View>
              );
            }
          },
        }}
      />
    </Tab.Navigator>
  );
}

const Stack = createNativeStackNavigator();
function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Mytabs" component={Mytabs} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default Navigation;

function ChatNavigation () {
  return (
    
    <Stack.Navigator screenOptions={{headerShown:false}} >
      <Stack.Screen name='Chats' component={Chats} />
    </Stack.Navigator>
    
  )
}


