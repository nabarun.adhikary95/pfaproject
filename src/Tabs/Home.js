import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Button} from 'react-native-elements';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
import Feather from 'react-native-vector-icons/Feather';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Card from '../Tabs/Card/Card';
import CardTwo from '../Tabs/Card/CardTwo';
import ImageCardOne from '../Tabs/imageCard/imageCardOne';
import ImageCardTwo from '../Tabs/imageCard/ImageCardTwo';
import VideoPlayer from 'react-native-video-player';
const Home = () => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginBottom: rw(15),
    },
    header: {
      backgroundColor: '#065659',
      height: rh(25),
      borderBottomLeftRadius: 40,
      borderBottomRightRadius: 40,
    },
    subheader: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
    headertext: {
      color: '#fff',
      justifyContent: 'center',
      alignItems: 'center',
    },
    containertext: {
      fontSize: rf(2),
      paddingVertical: 6,
      fontStyle: 'normal',
      color: '#000',
    },
    livedemoText: {
      color: '#000',
      fontSize: 12,
      lineHeight: 21,
      fontWeight: '300',
    },
    notificationIcon: {
      justifyContent: 'center',
      borderRadius: 30,
      borderColor: '#ffe',
      borderStyle: 'solid',
      borderWidth: 2,
    },
    imageContainer: {
      backgroundColor: '#93eaed',
      height: rh(20),
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 20,
      flexDirection: 'row',
      paddingHorizontal: rw(12),
    },
    imageView: {
      height: rh(20),
      width: rw(20),
      justifyContent: 'center',
      alignItems: 'center',
    },
    liveDemo: {
     
        color: '#065659',
        fontSize: 14,
        lineHeight: 21,
        fontWeight: 'bold',
      
    },
    buttonStyle:{
      
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
      
    }
  });
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        // showsVerticalScrollIndicator={true}
        style={{marginHorizontal: 10}}>
        <View style={styles.header}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Feather
              name="align-center"
              color={'#fff'}
              style={{marginLeft: rw(0.5), padding: 15}}
              size={30}
            />
            <View style={styles.subheader}>
              <Text style={styles.subheader}>sell your home</Text>
              <Fontisto
                name="bell-alt"
                color={'#5c7d80'}
                style={{padding: 10}}
                size={rw(5)}
              />
            </View>
          </View>
          <View style={{paddingVertical: rw(8), paddingLeft: rw(5)}}>
            <Text style={{fontSize: rf(2.8)}}> HOME </Text>
            <Text style={{fontSize: rf(2), marginLeft: rw(2)}}>
              By Champion Lender
            </Text>
          </View>
        </View>
        <View style={{paddingLeft: 10, marginVertical: 5}}>
          <Text style={styles.containertext}>Get Started With</Text>
          <ScrollView horizontal={true}>
            <View
              style={{marginVertical: 15, flexDirection: 'row'}}
              //    horizontal={true} contentContainerStyle ={{flexDirection:"row" }}
            >
              <View style={{marginRight: rw(-19)}}>
                <Card />
              </View>
              <View style={{marginRight: rw(-19)}}>
                <CardTwo />
              </View>
            </View>
          </ScrollView>
          {/* <MainCard /> */}
          <Text style={{color: '#065659', fontWeight: 'normal', fontSize: 13}}>
            See More
          </Text>
        </View>
        <ScrollView horizontal={true}>
          <View style={{marginVertical: 15, flexDirection: 'row'}}>
            <View style={{marginRight: 5}}>
              <ImageCardOne />
            </View>
            <ImageCardTwo />
          </View>
        </ScrollView>
        <View style={{ marginVertical: 5}}>
          <Text style={styles.containertext}>Upcoming Webinars</Text>
          <View style={{paddingTop: 10}}>
            <VideoPlayer
              video={{
                uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
              }}
              videoWidth={1600}
              videoHeight={900}
              thumbnail={require('../../src/assets/image2.jpeg')}
              style={{borderRadius: 20}}
            />
          </View>
          <View style={{margin: 15,marginHorizontal:0}}>
            <View>
          <Text
            style={styles.liveDemo}>
            Live Demo: Home Buying Sources
          </Text>
          <View >
            <Text style={styles.livedemoText}>
              Join us any day for live demo of Home Buying Source
            </Text>
          </View>
          <View style={{marginTop: 5}}>
            <Text style={styles.livedemoText}>
              {' '}
              Time 11:00 AM PT | 2:00 PM ET{' '}
            </Text>
          </View>
          <View style={{marginVertical: 1}}>
            <Text style={styles.livedemoText}>
              {' '}
              Duration: 30 minutes plus live Q& A
            </Text>
          </View>
          </View>
          <View
            style={styles.buttonStyle}>
            <Button
              containerStyle={{width: rw(45)}}
              buttonStyle={{backgroundColor: '#065659', borderRadius: 35}}
              title={'Register Now'}
            />
            <View style={styles.notificationIcon}>
              <MaterialIcons
                name="notifications-active"
                size={35}
                color="#065659"
              />
            </View>
          </View>
          <View style={styles.imageContainer}>
            <View>
              <Text style={{color: '#000'}}>
                Fir properties which match your budged
              </Text>
              <Text style={{color: '#000'}}>With Affordabilty Calculator</Text>
              <TouchableOpacity style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', paddingTop: 10}}>
                  <Text style={{color: '#183233', paddingRight: 10}}>
                    Match Now
                  </Text>
                  <AntDesign
                    name="arrowright"
                    size={20}
                    color={'#183233'}
                    style={{justifyContent: 'center', alignItems: 'center'}}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.imageView}>
              <Image
                source={require('../assets/house.png')}
                style={{height: 70, width: 70}}
              />
            </View>
          </View>
        </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default Home;
