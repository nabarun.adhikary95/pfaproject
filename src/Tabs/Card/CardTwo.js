import React from 'react';
import {SafeAreaView, View, Text, StyleSheet, Image} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
const CardTwo = () => {
  const styles = StyleSheet.create({
    title: {
      fontSize: 16,
      fontWeight: "bold",
      color: '#000',
      paddingBottom: 5,
      //   maxWidth: '50%',
      paddingTop: rh(2),
    },
    subTitle: {
      fontSize: 10,
      lineHeight: 21,
      fontWeight: "bold",
      color: '#000',
      //   maxWidth: '50%',
    },
    mainContainer: {
      backgroundColor: '#fff',
      borderRadius: 20,
      justifyContent: 'center',
      padding: 10,
      width: '70%',
      paddingLeft: 15,
      height:rh(25)
    },
    iconStyle: {
      backgroundColor: '#fff',
      borderRadius: 8,
      marginRight: rw(45),
      alignItems: 'center',
      justifyContent: 'center',
      height: rh(6),
      width:rw(10)
    }
  });

  return (
    <View
      style={styles.mainContainer}>
      <View
        style={styles.iconStyle}>
        <Image
          source={require('../../assets/fund.png')}
          style = {{height:30,width:30}} 
        />
      </View>

      <View>
        <Text style={styles.title}>Home Stash</Text>
        <Text style={styles.subTitle}>
          Discover and join communities of like minded people
        </Text>
      </View>
    </View>
  );
};
export default CardTwo;
