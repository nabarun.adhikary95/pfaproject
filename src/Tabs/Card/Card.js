import React from 'react';
import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
const Cards = () => {
  const styles = StyleSheet.create({
    title: {
      fontSize: 16,
      fontWeight: "bold",
      color: '#fff',
      paddingBottom: 5,
      //   maxWidth: '50%',
      paddingTop: rh(2),
    },
    subTitle: {
      fontSize: 10,
      lineHeight: 21,
      fontWeight: "bold",
      color: '#fff',
      //   maxWidth: '50%',
    },
    maincontainer: {
      backgroundColor: '#065659',
        borderRadius: 20,
        justifyContent: 'center',
        padding: 10,
        width: '70%',
        paddingLeft: 15,
    }, 
    iconstyle: {
      backgroundColor: '#fff',
      borderRadius: 8,
      marginRight: rw(45),
      alignItems: 'center',
      justifyContent: 'center',
      height: rh(6),
      width:rw(10),
    }
  });

  return (
    <View
      style={styles.maincontainer}>
      <View
        style={styles.iconstyle}>
        <MaterialCommunityIcons
          size={25}
          color={'#065659'}
          name="slot-machine-outline"
        />
      </View>

      <View>
        <Text style={styles.title}>Home Match</Text>
        <Text style={styles.subTitle}>
          Discover and join communities of like minded people
        </Text>
      </View>
    </View>
  );
};
export default Cards;
