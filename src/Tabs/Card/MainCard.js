import React from 'react';
import {SafeAreaView, Text, View, StyleSheet, ScrollView} from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf} from 'react-native-responsive-dimensions';
  import Card from '../../Tabs/Card/Card';
  import CardTwo from '../../Tabs/Card/CardTwo';

  const MainCard = () => {
return (
    <SafeAreaView>
        <ScrollView horizontal={true} contentContainerStyle ={{flexDirection:"row" }}>
            <View style={{marginRight:rw(-19)}} >
              <Card />
            </View>
            <View style={{marginRight:rw(-19)}}>
              <CardTwo />
            </View>
          </ScrollView>
    </SafeAreaView>
)
  }
  export default MainCard;