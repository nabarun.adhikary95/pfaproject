import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {Input} from 'react-native-elements';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Avatar} from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import IncomeingChat from '../Tabs/IncomeingChat';
import OutGoingChat from '../Tabs/OutGoingChat';
const Chats = ({navigation}) => {
  const styles = StyleSheet.create({
    mainbox: {
      flex: 1,
      marginHorizontal: 5,
      backgroundColor: '#065659',
      marginBottom: rh(15),
    },
    headerview: {backgroundColor: '#065659', height: rh(15)},
    headericon: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 25,
    },
    headerText: {
      color: '#FFF',
      fontSize: 17,
      fontWeight: 'bold',
      paddingHorizontal: 10,
    },
    inputView: {
      width: rw(80),
      borderWidth: 0.5,
      borderColor: '#065659',
      borderRadius: 30,
      height: rh(7),
      alignItems: 'center',
    },
    ScrollView: {
      overflow: 'scroll',
      backgroundColor: '#fff',
      borderTopLeftRadius: 25,
      position: 'relative',
      borderTopRightRadius: 25,
    },
    inputfield: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#fff',
      position: 'relative',
      height: rh(7),
    },
  });
  return (
    <SafeAreaView style={styles.mainbox}>
      <View>
        <View style={styles.headerview}>
          <View style={styles.headericon}>
            <AntDesign
              style={{marginHorizontal: 5}}
              onPress={() => {
                navigation.goBack();
              }}
              name="arrowleft"
              size={35}
              color="#fff"
            />
            <Avatar
              size={50}
              rounded
              source={{uri: 'https://randomuser.me/api/portraits/women/57.jpg'}}
              containerStyle={{backgroundColor: 'grey'}}
              avatarStyle={{borderColor: '#fff', borderWidth: 2}}
            />
            <Text style={styles.headerText}> Amma Watson </Text>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 10,
                alignContent: 'space-between',
              }}>
              <Feather
                style={{padding: 5, paddingRight: 10}}
                name="phone-call"
                color={'#fff'}
                size={20}
              />
              <FontAwesome
                style={{padding: 5}}
                name="video-camera"
                color={'#fff'}
                size={20}
              />
            </View>
          </View>
        </View>
      </View>
      <View style={styles.ScrollView}>
        <ScrollView>
          <IncomeingChat />
          <OutGoingChat />
          <IncomeingChat />
          <OutGoingChat />
          <IncomeingChat />
          <OutGoingChat />
          <IncomeingChat />
          <OutGoingChat />
          <IncomeingChat />
          <OutGoingChat />
          <IncomeingChat />
          <OutGoingChat />
        </ScrollView>
        <View style={styles.inputfield}>
          <View style={{padding: 5}}>
            <AntDesign name="pluscircleo" color={'#065659'} size={33} />
          </View>
          <TouchableWithoutFeedback onPress={() => Keyboard.addListener()}>
            <KeyboardAvoidingView
              behavior={Platform.OS === 'ios' ? 'height' : 'height'}
              style={styles.inputView}>
              <Input
                placeholder="Type Something"
                rightIcon={
                  <FontAwesome name="send-o" color={'#065659'} size={25} />
                }
              />
            </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default Chats;
