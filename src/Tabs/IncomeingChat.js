import React from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
import {Avatar} from 'react-native-elements';
const IncomeingChat = () => {
  const style = StyleSheet.create({
      containers:{padding: 10, paddingLeft: rw(10)},
    msgBox: {
      backgroundColor: '#c5d0e0',
      justifyContent: 'center',
      marginLeft: 10,
      height: rh(8),
      width: rw(50),
      alignItems: 'center',
      borderTopRightRadius: 30,
      borderBottomRightRadius: 30,
      borderBottomLeftRadius: 30,
      maxHeight: rw(30),
      maxWidth: rh(30),
    },
    timestyle: {
      color: '#000',
      fontSize: 10,
      alignSelf: 'flex-end',
      marginRight: rw(20),
    },
    
  });
  return (
    <View style={style.containers}>
      <View style={{flexDirection: 'row'}}>
        <Avatar
          size={50}
          rounded
          source={{uri: 'https://randomuser.me/api/portraits/women/57.jpg'}}
          containerStyle={{backgroundColor: 'grey'}}
          avatarStyle={{borderColor: '#065659', borderWidth: 2}}
        />

        <View style={style.msgBox}>
          <Text style={{color: '#000'}}>Hi Victor! I am Jacob Jones</Text>
        </View>
      </View>
      <Text style={style.timestyle}>11:30 PM</Text>
    </View>
  );
};
export default IncomeingChat;
