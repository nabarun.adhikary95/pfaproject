import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
const OutGoingChat = () => {
  const styles = StyleSheet.create({
      container:{
        marginRight: 20
      },
    msgbox: {
      backgroundColor: '#065659',
      justifyContent: 'center',
      marginLeft: 10,
      height: rh(8),
      width: rw(50),
      alignItems: 'center',
      borderTopLeftRadius: 30,
      borderBottomRightRadius: 30,
      borderBottomLeftRadius: 30,
    },
    timestyle: {
      color: '#000',
      fontSize: 10,
      alignSelf: 'center',
      marginLeft: rw(10),
    },
  });
  return (
    <View style={styles.container}>
      <View style={{alignSelf: 'flex-end'}}>
        <View style={styles.msgbox}>
          <Text style={{color: '#fff'}}>Hi Victor! I am Jacob Jones.</Text>
        </View>
      </View>
      <Text style={styles.timestyle}>11:30 PM</Text>
    </View>
  );
};
export default OutGoingChat;
