import React from 'react';
import {SafeAreaView, View, Text, StyleSheet, Image} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
const ImageCardTwo = () => {
  const styles = StyleSheet.create({
    title: {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#000',
      paddingBottom: 5,
      //   maxWidth: '50%',
      paddingTop: rh(2),
    },
    subTitle: {
      fontSize: 12,
      lineHeight: 21,
      fontWeight: 'bold',
      color: '#000',
      //   maxWidth: '50%',
    },
  });

  return (
    <View
      style={{
        backgroundColor: '#fff',
        minWidth: rw(65),
        minHeight: rh(30),
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
      }}>
      <View
        style={{
          //   backgroundColor: '#fff',
          borderRadius: 8,
        }}>
        <Image
          source={require('../../assets/image2.jpeg')}
          style={{
            height: rh(30),
            width: '100%',
            borderBottomRightRadius: 15,
            borderBottomLeftRadius: 15,
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
          }}
        />
      </View>

      <View
        style={{justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
        <Text style={styles.subTitle}>First Time Home Byers Course</Text>
      </View>
    </View>
  );
};
export default ImageCardTwo;
