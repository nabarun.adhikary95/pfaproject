import React from 'react';
import {SafeAreaView, View, Text, StyleSheet, Image} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
  responsiveFontSize as rf,
} from 'react-native-responsive-dimensions';
const ImageCardOne = () => {
  const styles = StyleSheet.create({
    title: {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#000',
      paddingBottom: 5,
      //   maxWidth: '50%',
      paddingTop: rh(2),
    },
    subTitle: {
      fontSize: 12,
      lineHeight: 21,
      fontWeight: 'bold',
      color: '#000',
      //   maxWidth: '50%',
    },
    maincontainer: {
      backgroundColor: '#fff',
      minWidth: rw(65),
      minHeight: rh(38),
      borderBottomRightRadius: 15,
      borderBottomLeftRadius: 15,
      borderTopLeftRadius: 15,
      borderTopRightRadius: 15,
    },
    imageStyle: {
      height: rh(30),
      width: '100%',
      borderBottomRightRadius: 15,
      borderBottomLeftRadius: 15,
      borderTopLeftRadius: 25,
      borderTopRightRadius: 25,
    },
  });

  return (
    <View style={styles.maincontainer}>
      <View>
        <Image
          source={require('../../assets/image.jpeg')}
          style={styles.imageStyle}
        />
      </View>

      <View
        style={{justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
        <Text style={styles.subTitle}>First Time Home Byers Course</Text>
      </View>
    </View>
  );
};
export default ImageCardOne;
